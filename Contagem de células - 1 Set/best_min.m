function best_pos = best_min(value, centroids, column)

        best_value = Inf(1);
        best_pos = 0;
        
        size_vector = length(centroids);
        
        for i=1:size_vector
            min = value - centroids(i,column);
            
            %Transforma em positivo
            min = sqrt(min^2);
            
            %Verifica se a distância é a menor obtida
            if min < best_value
                best_value = min;
                best_pos = i;
            end
            
            %disp([num2str(x) ' => ' num2str(centroids(i,1))]);
        end
        

end

