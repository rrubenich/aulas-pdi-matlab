function [centroids, cells] = extract_cells(image)

    % Obtem as células da imagem
    
    % Funcionamento: 
    % com a imagem já processada identifica na imagem centroids, retorna
    % uma array com todas coordenadas encontradas e o número de centroids

    
    props     = regionprops(image,'Centroid');
    centroids = cat(1, props.Centroid);
    cells     = length(centroids);

end

