image = imread('1.jpg');
image_gray = rgb2gray(image);

image_adapt = adapthisteq(image_gray);
image_adjust = imadjust(image_gray);

figure(1), subplot(3,2,1), imshow(image), title('Original');
figure(1), subplot(3,2,2), imhist(image_gray), title('Histograma');
figure(1), subplot(3,2,3), imshow(image_adapt), title('Adapt Hist');
figure(1), subplot(3,2,4), imhist(image_adapt), title('Histograma');
figure(1), subplot(3,2,5), imshow(image_adjust), title('Adjust');
figure(1), subplot(3,2,6), imhist(image_adjust), title('Histograma');