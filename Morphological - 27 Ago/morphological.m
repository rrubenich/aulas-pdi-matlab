img = imread('4.jpg');

se = strel('square', 10);

dilate = imdilate(img, se);
open = imopen(img, se);
erode = imerode(img, se);
close = imclose(img, se);
grandient = dilate - erode;


figure(1), imshow(img), title('Original');
figure(2), imshow(dilate), title('Dilate');
figure(3), imshow(open), title('Open');
figure(4), imshow(erode), title('Erode');
figure(5), imshow(close), title('Close');
figure(6), imshow(gradient), title('Gradient');

%%

im = imread('4.jpg');
img = im2bw(im);
se = strel('line', 15, 45);


dilate = imdilate(img, se);
open = imopen(img, se);
erode = imerode(img, se);
close = imclose(img, se);

figure(1), imshow(img), title('Original');
figure(2), imshow(dilate), title('Dilate');
figure(3), imshow(open), title('Open');
figure(4), imshow(erode), title('Erode');
figure(5), imshow(close), title('Close');